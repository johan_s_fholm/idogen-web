<?php
if( empty($_POST['name']) || 
	empty($_POST['email']) || 
	empty($_POST['message']) ||
	!filter_var($_POST['email'],FILTER_VALIDATE_EMAIL))
	{
		header("HTTP/1.1 500 Internal Server Error");
		exit;
	}
// Simple check for bots
if ($_POST['human_proof'] != '12')
{
	header("HTTP/1.1 404 Not Found");
	exit;
}
        
$name = $_POST['name'];
$email_address = $_POST['email'];
$message = $_POST['message'];
$to = "info@idogen.com";
$email_subject = "Contact form submitted by:" .  $name;
$email_body = 	"You have received a new message from Idogen website contact form. \n" .
				"Here are the details:\n" . 
				"Name: " . $name . "\n" .
                "Email: " . $email_address . "\n" .
				"Message: \n" . $message . "\n";
$headers = "To: " . $to . "\r\n";
$headers .= "From: " . $email_address . "\r\n";
$headers .= "Reply-To:" . $email_address . "\r\n";
      
// Only return true if mail actually was sent
if (mail($to,$email_subject,$email_body,$headers)){
		return true;
	} else {
		header("HTTP/1.1 503 Service Unavailable");
		exit;
	}
?>