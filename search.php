<?php 
// The html needed to display page according to site template
echo '<!DOCTYPE html>';
echo '<html lang="sv">';
// HTML head
echo '<head>';
echo '<meta charset="UTF-8">';
echo '<title>Idogen</title>';
//echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8">';
echo '<meta name="viewport" content="width=device-width, initial-scale=1">';
echo '<link rel="stylesheet" href="./css/style.css" type="text/css">';
echo '<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=latin,latin-ext"  type="text/css">';
echo '<link rel="stylesheet" href="./css/bootstrap.css" >';
echo '<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-theme.min.css">';
echo '<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">';
echo '<link rel="stylesheet" href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css">';
echo '<link rel="shortcut icon" href=".\favicon.ico">';
echo '<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>';
echo '<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>';
echo '<script src="./js/jqBootstrapValidation.js"></script>';
echo '</head>';
// Navbar
echo '<nav class="navbar navbar-default" role="navigation">';
echo '<div class="container-fluid"  id="menu">';
echo '<!-- Brand and toggle get grouped for better mobile display -->';
echo '<div class="row">';
echo '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-2"></div>';
echo '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-8">';
echo '<div class="navbar-header">';
echo '<a href="index.html"><img src="images/logo.png" width="210" height="65" id="logo"></a>';
echo '<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">';
echo '<span class="sr-only">Toggle navigation</span>';
echo '<span class="icon-bar"></span>';
echo '<span class="icon-bar"></span>';
echo '<span class="icon-bar"></span>';
echo '</button>';
echo '</div>';
echo '<div class="searchrowupper">';
echo '<a href="en/index.html">';
echo '<img src="./images/uk_flag.jpg" height="20" id="flag_pic">';
echo '</a>';
echo '<form action="search.php" method="post" class="navbar-form nav navbar-nav navbar-right" role="search" id="searchbox">';
echo '<div class="input-group add-on">';
echo '<input type="text" class="form-control" placeholder="Sök..." name="search_string" id="search_string">';
echo '<div class="input-group-btn">';
echo '<button class="btn btn-default" type="submit" id="searchbutton"><i class="glyphicon glyphicon-search"></i></button>';
echo '</div>';
echo '</div>';
echo '</form>';
echo '</div>';
echo '</div><!--col-->';
echo '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-2"></div>';
echo '</div><!--row-->';
echo '<!-- Collect the nav links, forms, and other content for toggling -->';
echo '<div class="row">';
echo '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-2"></div>';
echo '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-8"  id="links">';
echo '<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">';
echo '<ul class="nav navbar-nav" id="links">';
echo '<li><a href="index.html" id="sections">Hem</a></li>';
echo '<li class="dropdown">';
echo '<a href="#" class="dropdown-toggle" data-toggle="dropdown" id="sections">Om Idogen</a>';
echo '<ul class="dropdown-menu" role="menu">';
echo '<li><a href="about.html">Affärskoncept</a></li>';
echo '<li><a href="management.html">Ledningsgrupp</a></li>';
echo '<li><a href="board.html">Styrelse</a></li>';
echo '<li><a href="advisoryboard.html">Vetenskapliga rådgivare</a></li>';
echo '<li><a href="career.html">Karriär</a></li>';
echo '</ul>';
echo '</li>';
echo '<li class="dropdown">';
echo '<a href="#" class="dropdown-toggle" data-toggle="dropdown" id="sections">Forskning och utveckling</a>';
echo '<ul class="dropdown-menu" role="menu">';
echo '<li><a href="research.html">Forskningsöversikt</a></li>';
echo '<li><a href="projectportfolio.html">Projektportfölj</a></li>';
echo '<li><a href="collaborations.html">Vetenskapliga samarbeten</a></li>';
echo '<li><a href="publications.html">Vetenskapliga artiklar</a></li>';
echo '</ul>';
echo '</li>';
echo '<li class="dropdown">';
echo '<a href="#" class="dropdown-toggle" data-toggle="dropdown" id="sections">Investerare</a>';
echo '<ul class="dropdown-menu" role="menu">';
echo '<li><a href="reports.html">Rapporter</a></li>';
echo '<li><a href="stockinfo.html">Aktieinformation</a></li>';
echo '<li><a href="presentations.html">Presentationer</a></li>';
echo '<li><a href="corporategovernance.html">Bolagsstyrning</a></li>';
echo '<li><a href="financialcalendar.html">Finansiell kalender</a></li>';
echo '<li><a href="investorscontact.html">Investerarkontakt</a></li>';
echo '</ul>';
echo '</li>';
echo '<li><a href="news.html">Nyheter</a></li>';
echo '<li><a href="contact.html">Kontakta oss</a></li>';
echo '</ul>';
echo '</div><!-- /.navbar-collapse -->';
echo '</div><!--col-->';
echo '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-2"></div>';
echo '</div><!--row-->';
echo '</div><!-- /.container-fluid -->';
echo '</nav>';
echo '<div class="container" id="welcomecontainer">';
//echo '<div class="row">';
echo '<div id="welcomeheading">';
$found=false;
if(!empty($_POST['search_string'])){
	//$search_string = escapeshellarg($_POST['search_string']);
	$search_string = $_POST['search_string'];
	$search_string_without_quotes = substr(substr($search_string, 1), 0 , -1);
	$search_string_w_html = $search_string_without_quotes . '.html';
	echo 'Du sökte efter: '. $search_string;
	echo '</div>'; // div welcomeheading
	echo '<hr id="line">';
	echo '<div id="welcometext">';
	echo '<p align="justify">';
	foreach (glob("*.html") as $filename){
		$file_content = file_get_contents($filename,true);
		$file_split_1 = explode('heading">',$file_content);
		$file_split_2 = $file_split_1[1];
		$file_split_3 = explode('<div id="foo',$file_split_2);
		$file_split_4 = strip_tags($file_split_3[0]);
		if (stripos($file_split_4,$search_string_without_quotes) !== false){
			echo "Sida: <a href='" . $filename . "'>" . $filename . "</a>" . "<br>";
			$search_result_to_show = explode($search_string_without_quotes,$file_split_4);
			$search_result_before = $search_result_to_show[0];
			$search_result_after = $search_result_to_show[1];
			$search_results_before_to_show_sliced = array_slice(explode(" ", $search_result_before),-10);
			$search_results_after_to_show_sliced = array_slice(explode(" ", $search_result_after),0,10);
			$final_text_to_show = substr(implode(" ",$search_results_before_to_show_sliced),0,-1) . "<b>" . $search_string . "</b>" . substr(implode(" ",$search_results_after_to_show_sliced),1);
			echo "... " . $final_text_to_show . "...";
			echo '<br>';
			$found = true;
		} 
	}
	if (!$found){
		echo 'Din sökning gav inga träffar.<br>';
	}
	echo '<br>';	
} else if (!$found){ // If nothing was entered in search box
		echo 'Din sökning gav inga träffar<br>';
}

echo '</div>';
//echo '</div>'; // div class row
echo '</div>'; // div welcomecontainer
// footer
echo '<div id="footer">';
echo '<div class="container">';
echo '<div class="row">';
echo '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">';
echo '<div id="bottommenu" align="center">';
echo '<ul>';
echo '<li id="copy2"><a href="index.html">Hem</a> | </li>';
echo '<li id="copy2"><a href="about.html">Om Idogen</a> | </li>';
echo '<li id="copy2"><a href="research.html">Forskning och utveckling</a> | </li>';
echo '<li id="copy2"><a href="reports.html">Investerare</a> | </li>';
echo '<li id="copy2"><a href="news.html">Nyheter</a> | </li>';
echo '<li id="copy2"><a href="contact.html">Kontakta oss</a></li>';
echo '</ul></div></div></div>';
echo '<div class="row">';
echo '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">';
echo '<p align="center" id="company">Idogen AB - Scheelevägen 2, 22381, Lund</p>';
echo '<p align="center">Tel: 046 - 275 63 30 - info at idogen dot com</p>';
echo '<p align="center">Sökfunktion skriven av Johan Säfholm</p>';
echo '<p align="center" id="copy">Copyright 2014 - All rights reserved</p>';
echo '</div></div></div></div></body></html>';
return;

function strip_tags_for_walk(&$item1){
    $item1 = strip_tags($item1);
}
function remove_link_for_walk(&$item2){
	$exploded_result = explode(".html:",$item2);
	$item2 = $exploded_result[1];
}
/*
function make_array_with_link_as_key(&$item3,$item4){
	$exploded_result = explode(":",$item4);
	$item3($exploded_result[0] => $exploded_result[1]);
}
*/
?>